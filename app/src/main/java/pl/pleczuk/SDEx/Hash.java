package pl.pleczuk.SDEx;


public abstract class Hash {


    public static int  DIGEST_SIZE = (256 / 8);
    int IV_SIZE = 8; //for sha2

    public Hash() {

    }


    public abstract void init();

    public abstract void init(int[] iv, int iv_size);

    public abstract int[] init_vector();

    public abstract void update(byte[] data, int offset, int length);

    public abstract void finishDigest(byte out[], int  outOffset);


    public Hash(int _digest_size) {
        DIGEST_SIZE = _digest_size;
    }



    String digest_to_string(byte[] digest) {
        StringBuffer ss = new StringBuffer();

        for (int i = 0; i < DIGEST_SIZE; i++) {
            String t = String.format("%x",digest[i]).toString();
            ss.append(t);
        }
        System.out.println(ss.toString());
        return  ss.toString();


    }
}
