package pl.pleczuk.SDEx;

public class DMChainHash extends ChainHash {

    protected void updateInitVector() {
        char[] _niv = Converter.convert(_next_init_vector).toString().toCharArray();
        char[] _lh = Converter.convert(_last_hash).toString().toCharArray();
        for (int i = 0; i < hash.IV_SIZE; i++) {
            _next_init_vector[i] = _next_init_vector[i] ^ _last_hash[i];
        }
    }

    public DMChainHash(Hash _hash) {
        super(_hash);
    }
}
