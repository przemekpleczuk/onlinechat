package pl.pleczuk.SDEx;


import java.nio.ByteBuffer;
import java.nio.IntBuffer;

public class ChainHash {

    static int CHAIN_BLOCK_SIZE;
    static int DIGEST_SIZE;

    protected int _iteration_count;
    protected int[] _last_hash;
    protected int[] _next_init_vector;
    Hash hash;


    private static final int[] SHA256_h_0 = new int[]{0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19};

    protected ChainHash(Hash _hash) {
        hash = _hash;
        DIGEST_SIZE = _hash.DIGEST_SIZE;
        CHAIN_BLOCK_SIZE = 2 * _hash.DIGEST_SIZE;//_hash.IV_SIZE;
        _last_hash = new int[_hash.IV_SIZE];
        _next_init_vector = new int[_hash.IV_SIZE];
        _iteration_count = 0;

        _hash.init();

        System.arraycopy(_hash.init_vector(), 0,_next_init_vector,0, _hash.IV_SIZE);



    }

    protected void updateInitVector() {

    }



    public void hashNextBlock(String input) {
        byte[] in = input.getBytes();
        hashNextBlock(in, in.length);
    }

    public void hashNextBlock(byte[] input, int length) {
        if (length == -1) {
            length = CHAIN_BLOCK_SIZE;
        }

        byte[] digest = new byte[hash.DIGEST_SIZE];
        hash.init(_next_init_vector, hash.IV_SIZE); //8 - for sha
        hash.update(input, 0, length);

        System.arraycopy(hash.init_vector(),0, _last_hash,0, hash.IV_SIZE);

        if (length < CHAIN_BLOCK_SIZE) {
            hash.finishDigest(digest,0);
            ByteBuffer bufferedDigest = ByteBuffer.allocate(hash.DIGEST_SIZE * 4);


            IntBuffer intBuffer = bufferedDigest.asIntBuffer();
            bufferedDigest.put(digest);

            intBuffer.get(_last_hash);
        }

        updateInitVector();
        _iteration_count += 1;
    }


    public void hashNextBlock(String inputA, String inputB) {
        hashNextBlock(inputA + inputB);
    }

    public void clear() {
        hash.init();
        _iteration_count = 0;
    }

    public int[] init_vector() {
        return _next_init_vector;
    }
    public int[] last_hash() {
        return _last_hash;
    }

}
