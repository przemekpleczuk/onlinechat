package pl.pleczuk.SDEx;

import java.io.*;

class Converter {

    private static final int BYTES_IN_INT = 4;

    private Converter() {}

    private static boolean isEmpty(int[] array) {
        return array.length==0 ? true : false;
    }
    private static boolean isEmpty(byte[] array) {
        return array.length==0 ? true : false;
    }

    public static byte [] convert(int [] array) {
        if (isEmpty(array)) {
            return new byte[0];
        }

        return writeInts(array);
    }

    public static int [] convert(byte [] array) {
        if (isEmpty(array)) {
            return new int[0];
        }

        return readInts(array);
    }

    private static byte [] writeInts(int [] array) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream(array.length * 4);
            DataOutputStream dos = new DataOutputStream(bos);
            for (int i = 0; i < array.length; i++) {
                dos.writeInt(array[i]);
            }

            return bos.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static int [] readInts(byte [] array) {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(array);
            DataInputStream dataInputStream = new DataInputStream(bis);
            int size = array.length / BYTES_IN_INT;
            int[] res = new int[size];
            for (int i = 0; i < size; i++) {
                res[i] = dataInputStream.readInt();
            }
            return res;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}