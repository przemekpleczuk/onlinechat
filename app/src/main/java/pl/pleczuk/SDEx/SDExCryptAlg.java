package pl.pleczuk.SDEx;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.charset.StandardCharsets;

import static pl.pleczuk.SDEx.ChainHash.CHAIN_BLOCK_SIZE;
import static java.lang.Math.min;

public class SDExCryptAlg {

    private int block_crypted;

    protected DMChainHash chainHash;
    protected int[] H_IV;
    protected int[] H_U;

    protected char[] inputOdd;
    protected char[] inputEven;


    protected void divide_input_into_subblocks(char[] inputBlock, int input_size){

        inputEven = new  char[CHAIN_BLOCK_SIZE / 2];
        inputOdd = new char[CHAIN_BLOCK_SIZE / 2];

        //For empty block
        System.arraycopy(inputBlock, 0,  inputOdd, 0, Math.min(input_size, CHAIN_BLOCK_SIZE/2));

        if (input_size > CHAIN_BLOCK_SIZE / 2) {
            System.arraycopy(inputBlock, CHAIN_BLOCK_SIZE / 2, inputEven, 0, Math.min(input_size - CHAIN_BLOCK_SIZE / 2, CHAIN_BLOCK_SIZE / 2));
        }
    }



    protected void first_block_cypher(char[] inputBlock, int size, char[] outputBlock) {
        divide_input_into_subblocks(inputBlock,size);
        int block_count = CHAIN_BLOCK_SIZE / 2;

        ByteBuffer h = ByteBuffer.allocate(H_IV.length * 4);
        IntBuffer ib_h = h.asIntBuffer();
        ib_h.put(H_IV);

        ByteBuffer h_u = ByteBuffer.allocate(H_U.length * 4);
        IntBuffer ib_h_u = h_u.asIntBuffer();
        ib_h_u.put(H_U);

        ByteBuffer iv = ByteBuffer.allocate(chainHash.init_vector().length * 4);
        IntBuffer ib_iv = iv.asIntBuffer();
        ib_iv.put(chainHash.init_vector());


        for (int i = 0; i < block_count; i++) {
            outputBlock[i] = (char) (inputOdd[i] ^ h.get(i) ^ iv.get(i));
            outputBlock[block_count + i]= (char)(inputEven[i] ^ h.get(i) ^ h_u.get(i));
        }
        chainHash.hashNextBlock(new String(inputBlock));
    }

    protected void block_cypher(char[] inputBlock, int size, char[] outputBlock) {
        divide_input_into_subblocks(inputBlock,size);
        int block_count = CHAIN_BLOCK_SIZE / 2;


        ByteBuffer init_vector = ByteBuffer.allocate(chainHash.init_vector().length * 4);
        IntBuffer ib_init_vector = init_vector.asIntBuffer();
        ib_init_vector.put(chainHash.init_vector());

        ByteBuffer last_hash = ByteBuffer.allocate(chainHash.last_hash().length * 4);
        IntBuffer ib_last_hash = last_hash.asIntBuffer();
        ib_last_hash.put(chainHash.last_hash());

        ByteBuffer h_u = ByteBuffer.allocate(H_U.length *4);
        IntBuffer ib_h_u = h_u.asIntBuffer();
        ib_h_u.put(H_U);



        for (int i = 0; i < block_count; i++) {

            outputBlock[i]=(char) (inputOdd[i] ^ init_vector.get(i));
            outputBlock[block_count + i]=(char)(inputEven[i]^ last_hash.get(i) ^ h_u.get(i));

        }
        chainHash.hashNextBlock(new String(inputBlock));
    }

    protected void first_block_decypher(char[] inputBlock, int size, char[] outputBlock){
        divide_input_into_subblocks(inputBlock, size);
        int block_count = CHAIN_BLOCK_SIZE / 2;


        ByteBuffer h = ByteBuffer.allocate(H_IV.length * 4);
        IntBuffer ib_h = h.asIntBuffer();
        ib_h.put(H_IV);


        ByteBuffer h_u = ByteBuffer.allocate(H_U.length * 4);
        IntBuffer ib_h_u = h_u.asIntBuffer();
        ib_h_u.put(H_U);

        ByteBuffer iv = ByteBuffer.allocate(chainHash.init_vector().length * 4);
        IntBuffer ib_iv = iv.asIntBuffer();
        ib_iv.put(chainHash.init_vector());



        for (int i = 0; i < block_count; i++) {
            outputBlock[i] = (char)(inputOdd[i] ^ h.get(i) ^ iv.get(i));
            outputBlock[block_count + i] = (char)(inputEven[i] ^ h.get(i) ^ h_u.get(i));
        }

        chainHash.hashNextBlock(new String(outputBlock));
    }


    protected void block_decypher(char[] inputBlock, int size, char[] outputBlock){
        divide_input_into_subblocks(inputBlock, size);
        int block_count = CHAIN_BLOCK_SIZE / 2;


        ByteBuffer init_vector = ByteBuffer.allocate(chainHash.init_vector().length * 4);
        IntBuffer ib_init_vector = init_vector.asIntBuffer();
        ib_init_vector.put(chainHash.init_vector());

        ByteBuffer last_hash = ByteBuffer.allocate(chainHash.last_hash().length * 4);
        IntBuffer ib_last_hash = last_hash.asIntBuffer();
        ib_last_hash.put(chainHash.last_hash());

        ByteBuffer h_u = ByteBuffer.allocate(H_U.length *4);
        IntBuffer ib_h_u = h_u.asIntBuffer();
        ib_h_u.put(H_U);



        for (int i = 0; i < block_count; i++) {

            outputBlock[i] = (char)(inputOdd[i] ^ init_vector.get(i));
            outputBlock[block_count + i] = (char)(inputEven[i] ^ last_hash.get(i) ^ h_u.get(i));
        }

        chainHash.hashNextBlock(new String(outputBlock));
    }



    public String crypt(String message) throws IOException {
        long message_len = message.length();
        char[] outputBlock = new char[CHAIN_BLOCK_SIZE];
        StringBuilder ss = new StringBuilder();

        while (message_len > 0) {
            int size = (int)Math.min((long)CHAIN_BLOCK_SIZE, message_len);
            if (block_crypted < 1) {
                first_block_cypher(message.substring(0,size).toString().toCharArray(), size , outputBlock);
            }
            else {
                block_cypher(message.substring(block_crypted * CHAIN_BLOCK_SIZE, block_crypted * CHAIN_BLOCK_SIZE+size).toString().toCharArray(), size, outputBlock);
            }
            block_crypted++;

            ss.append(new String(outputBlock));
            message_len -= CHAIN_BLOCK_SIZE;
        }

        System.out.println("Block encrypted :" + block_crypted + " Last Block:" + message_len + CHAIN_BLOCK_SIZE);
        return  ss.toString();
    }

    public String decrypt(String message) {
        long message_len = message.length();
        char[] outputBlock = new char[CHAIN_BLOCK_SIZE];
        StringBuilder ss = new StringBuilder();
        while (message_len > 0) {
            int size = (int)min((long) CHAIN_BLOCK_SIZE, message_len);
            if (block_crypted < 1) {
                first_block_decypher(message.substring(0, size).toString().toCharArray(), size, outputBlock);
            }
            else {
                block_decypher(message.substring(block_crypted * CHAIN_BLOCK_SIZE, block_crypted * CHAIN_BLOCK_SIZE+size).toString().toCharArray(), size, outputBlock);
            }
            block_crypted++;
            String outputBlock2 = new String(outputBlock);
            ss.append(outputBlock2);
            message_len -= CHAIN_BLOCK_SIZE;
        }
        System.out.println("Block decrypted :" + block_crypted + " Last Block:" + message_len + CHAIN_BLOCK_SIZE);
        return ss.toString();
    }

    public SDExCryptAlg(Hash _hash, String IV, String U) {
        chainHash = new DMChainHash(_hash);
        block_crypted = 0;

        H_IV = new int[_hash.IV_SIZE];
        H_U = new int[_hash.IV_SIZE];
        inputOdd = new char[CHAIN_BLOCK_SIZE / 2];
        inputEven = new char[CHAIN_BLOCK_SIZE / 2];

        _hash.init();
        _hash.update(IV.getBytes(StandardCharsets.UTF_8), 0, IV.length());

        byte[] bh_iv = new byte[_hash.IV_SIZE*4];
        _hash.finishDigest(bh_iv,0);
        byte[] reversed_bh_iv = new byte[_hash.IV_SIZE*4];
        for (int i=0; i<bh_iv.length; i++){
            reversed_bh_iv[i] = bh_iv[i];
        }
        ByteBuffer bBuffer = ByteBuffer.wrap(reversed_bh_iv);

        IntBuffer iBuffer = bBuffer.asIntBuffer();
        iBuffer.get(H_IV);




        _hash.init();
        _hash.update(U.getBytes(StandardCharsets.UTF_8), 0, U.length());

        byte[] bh_u = new byte[_hash.IV_SIZE*4];

        _hash.finishDigest(bh_u,0);

        byte[] reversed_bh_u = new byte[_hash.IV_SIZE*4];
        for (int i=0; i<bh_u.length; i++){
            reversed_bh_u[i] = bh_u[i];
        }
        ByteBuffer byteBuffer = ByteBuffer.wrap(reversed_bh_u);
        IntBuffer intBuffer = byteBuffer.asIntBuffer();
        intBuffer.get(H_U);


        _hash.init();
    }

}
