package pl.pleczuk.SDEx;

import java.io.*;

public class SDEx {

    private String IV, U_H;

    public SDEx(){
        IV = "Ala_ma_kota";
        U_H = "Ala_nie_ma_kota";
    }

    public SDEx(String iv, String u_h){
        IV = iv;
        U_H = u_h;
    }



    public String cypherMessage(String msg) throws IOException {

        SDExCryptAlg crypt = new SDExCryptAlg(new Sha256(), IV, U_H);
        String encrypted = crypt.crypt(msg).substring(0, msg.length());

        return encrypted;
    }


    public String decryptMessage(String msg){

        SDExCryptAlg decrypt = new SDExCryptAlg(new Sha256(), IV, U_H);
        String decrypted = decrypt.decrypt(msg).substring(0, msg.length());

        return decrypted;
    }

}

