package pl.pleczuk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.codelab.friendlychat.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class PasswordActivity extends AppCompatActivity {

    EditText passEditText;
    Button passBtn;
    IV iv;

    public static final String PASSWORD = "Ala_nie_ma_kota";
    public static final String IV_CHAIN = "Ala_ma_kota";
    public static boolean isIV = false;
    public static boolean isHU = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        iv = new IV();


        passEditText = (EditText) findViewById(R.id.passwordEditText);
        passBtn = (Button) findViewById(R.id.passwordBtn);

        passBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String p = passEditText.getText().toString();


                if(PASSWORD.equals(p)) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra("U_PASSWORD", p);
                    isHU = true;
                    startActivity(intent);
                    finish();
                } else {
                    Log.d("LogOut", "Password is not properly!");
                }



                if (IV_CHAIN.equals(iv.getValue())) {
                    isIV = true;
                    Log.d("LogOut", "::IV:: " + iv.getValue());
                } else {
                    isIV = false;
                    Log.d("LogOut", "::!IV!:: " + iv.getValue());
                }

            }
        });




        passEditText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                DatabaseReference databaseReference = firebaseDatabase.getReferenceFromUrl("https://friendlychat-5daa3.firebaseio.com/");

                databaseReference.child("iv").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String temp = dataSnapshot.getValue(String.class);

                        iv.setValue(temp);
                        Log.d("LogOut", "onDataChange ---> " + temp);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }
}